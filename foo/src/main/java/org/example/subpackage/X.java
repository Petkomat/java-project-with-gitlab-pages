package org.example.subpackage;

public class X {
    /**
     * x1 value
     */
    public static String x1 = "a";

    /**
     * x2 value
     *
     * @see org.example.App#main(String[]) 
     */
    public static String x2 = "b";
}

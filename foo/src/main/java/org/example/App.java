package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    /**
     * THe main entrance
     * @param args x1, x2
     *
     * @see org.example.subpackage.X
     * @see org.example.subpackage.X#x1
     */
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
